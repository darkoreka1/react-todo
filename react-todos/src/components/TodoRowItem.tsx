import React from "react";

export const TodoRowItem: React.FC<{
    rowNumber: number,
    rowDescription: string,
     rowAssigned: string,
      deleteTodo: Function
}> = (props) => { //FC means we are returning a functional component
    
    return(
        <tr onClick={() => props.deleteTodo(props.rowNumber)}>
            <th scope='row'>{props.rowNumber}</th>
            <td>{props.rowDescription}</td>
            <td>{props.rowAssigned}</td>
        </tr>
    )
}
