
import React, {useState} from 'react';
import './App.css';
import {TodoTable} from './components/TodoTable';
import {NewTodoForm} from './components/NewTodoForm';

export const App = () => {

  const [showAddTodoForm, setShowAddTodoForm] = useState(false);

  const [todos, setTodos] = useState([ //todos-reference all the info inside useState, setTodos-update todos variable
    { rowNumber: 1, rowDescription: 'Feed puppy', rowAssigned: 'User one' },
    { rowNumber: 2, rowDescription: 'Water plants', rowAssigned: 'User two' },
    { rowNumber: 3, rowDescription: 'Make dinner', rowAssigned: 'User One' },
    { rowNumber: 4, rowDescription: 'Charge battery', rowAssigned: 'User One' },
  ]);

  const addTodo = (description: string, assigned: string) => {
    let rowNumber = 0;
    if (todos.length > 0) {
      rowNumber = todos[todos.length -1].rowNumber + 1;
    } else {
      rowNumber = 1;
    }
      const newTodo = { 
        rowNumber: rowNumber,
         rowDescription: description,
          rowAssigned: assigned 
        };
    
    setTodos(todos => [...todos, newTodo]); //...todos-destructure the todos that already exist and pass them in a new array=newTodo 
      };

    const deleteTodo = (deleteTodoRowNumber: number) => { 
      let filtered = todos.filter(function (value) {
        return value.rowNumber !== deleteTodoRowNumber;
      });
      setTodos(filtered);
    }
  

  return (
    <div className="mt-5 container">
      <div className="card">
        <div className="card-header">
          Your Todo's
        </div>
        <div className="card-body">
          <TodoTable todos={todos} deleteTodo = {deleteTodo} /> 
          <button onClick={() => setShowAddTodoForm(!showAddTodoForm)} className='btn btn-primary'>
            {/* if showAddTodoForm is true show the first statement, else show the second */}
            {showAddTodoForm ? 'Close new Todo' : 'New Todo'} 
          </button>
        {showAddTodoForm && 
        <NewTodoForm addTodo={addTodo}/>
        }
        </div>
      </div>
    </div>
  );
}

